package com.vwhale.dayloop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DayloopApplication {

    public static void main(String[] args) {
        SpringApplication.run(DayloopApplication.class, args);
    }
}
